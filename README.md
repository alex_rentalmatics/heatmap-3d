# HEAT MAP 3D #
Uses the deck.gl library to display a 3d heatmap based on the amount of coordinates in the same point in the map.

![Screenshot](print.png)
### How do I get set up? ###

```bash
npm install
npm start
```

### Datasets - until 26/nov/2021 ###
* json_array_july.json
* json_array.json (since fev/2021)